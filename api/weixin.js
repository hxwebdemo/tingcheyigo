import request from "@/common/request.js"

// 获取个人信息
export function wxUserInfo(data) {
	return request({
		url: '/wx/info',
		method: 'GET',
		data,
	})
}

// 获取手机号
export function getPhone(data) {
	return request({
		url: '/wx/phone',
		method: 'GET',
		data,
	})
}

// 登录
export function wxLogin(data) {
	return request({
		url: '/wx/login',
		method: 'GET',
		data,
	})
}

// 建议
export function fetchSuggestFeedback(data) {
	return request({
		url: '/feedback/add',
		method: 'post',
		data,
	})
}

// 更新
export function fetchUpDataInfo(data) {
	return request({
		url: '/wx/info/update',
		method: 'post',
		data,
	})
}

// 收藏or取消收藏
export function fetchCollect(data) {
	return request({
		url: '/userCenter/collectParing',
		method: 'post',
		data
	})
}

// 收藏列表
export function fetchCollectList(data) {
	return request({
		url: '/userCenter/collectList',
		method: 'get',
		data
	})
}