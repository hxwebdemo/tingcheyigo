import request from "@/common/request.js"

export function parkInfoList(data) {
	return request({
		url: '/parkingInformation/list',
		method: 'POST',
		data,
	})
}

// 省市区数据
export function regionList(data) {
	return request({
		url: '/city/list',
		method: 'GET',
		data,
	})
}



export function parkInfoMyList(data) {
	return request({
		url: '/parkingInformation/myList',
		method: 'POST',
		data,
	})
}
// 详情
export function parkInfoDetail(data) {
	return request({
		url: `/parkingInformation/getById`,
		method: 'post',
		data,
	})
}

// 删除
export function parkInfoDetailDelete(id) {
	return request({
		url: `/parkingInformation/delete/${id}`,
		method: 'post',
	})
}


// 新增
export function upLoadInfo(data) {
	return request({
		url: '/parkingInformation/save',
		method: 'post',
		data,
	})
}


// 修改
export function upDateInfo(data) {
	return request({
		url: '/parkingInformation/update',
		method: 'post',
		data,
	})
}

