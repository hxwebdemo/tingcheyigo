export const baseURL = 'https://api.carparklink.cn/biker-alliance/app'

export default (options) => {
	return new Promise((resolve, reject) => {
		uni.request({
			url: baseURL + options.url,
			method: options.method || 'GET',
			data: options.data || {},
			header: {
				'Content-Type': 'application/json',
				'token': uni.getStorageSync('token')
			},
			success(res) {
				// console.log("请求拦截 res: ",res);
				if (res.data.code === 0) {
					resolve(res.data);
				} else if (res.data.code === 401) {
					console.log("拦截成功: ");
					uni.reLaunch({
						url:'/pages/userInfo/index?token=1'
					})
				}
			},
			fail(err) {
				reject(err);
			},
			complete() {
				uni.hideLoading();
			}
		});
	});
};
